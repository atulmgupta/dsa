package com.atul.algorithm.tree;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PreOrderTraversalTest {
    @Test
    public void createTree(){
      TreeNode root = new TreeNode(1);
      root.left = new TreeNode(2);
        root.right = new TreeNode(8);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(15);
        System.out.println();
    }

}