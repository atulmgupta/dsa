package com.atul.algorithm.tree;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class BinarySearchTreeTest {

//    @Test
//    public void insertIterative() {
//        BinarySearchTree bst = new BinarySearchTree();
//
//        bst.insertIterative(6);
//        bst.insertIterative(4);
//        bst.insertIterative(9);
//        bst.insertIterative(5);
//        bst.insertIterative(2);
//        bst.insertIterative(8);
//        bst.insertIterative(12);
//        bst.insertIterative(10);
//        bst.insertIterative(14);
//
//        bst.printTree(bst.getRoot());
//    }
    @Test
    public void insertRecursive() {
        BinarySearchTree bst = new BinarySearchTree();

        bst.addRecursive(6);
        bst.addRecursive(4);
        bst.addRecursive(9);
        bst.addRecursive(5);
        bst.addRecursive(2);
        bst.addRecursive(8);
        bst.addRecursive(12);
        bst.addRecursive(10);
        bst.addRecursive(14);

        bst.printTree(bst.getRoot());
    }

}