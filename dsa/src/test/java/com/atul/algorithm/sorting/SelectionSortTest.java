package com.atul.algorithm.sorting;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class SelectionSortTest {

    @Test
    public void doSelectionSort() {
        int[] data = {8, 5, 7, 3, 2};
        SelectionSort selectionSort = new SelectionSort();
        selectionSort.doSelectionSort(data);
        for (int d : data
        ) {
            System.out.print(d + " ");
        }
    }
}