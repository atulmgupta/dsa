package com.atul.structure;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atul.structure.trie.Trie;
import com.atul.structure.trie.TrieNode;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
public class TrieTest {

	@Test
	public void insertTrie() {
		Trie test = new Trie();

		String data = "Its vanished trees the trees that had made way for Gatsbys house"
					+ " had once pandered in whispers to the last and greatest of all human dreams for a transitory enchanted moment "
					+ "man must have held his breath in the presence of this continent compelled into an aesthetic contemplation he "
					+ "neither understood nor desired face to face for the last time in history with something commensurate to his capacity for wonder";
		String[] texts = data.split(" ");
		
		String keys[] = { "the", "there", "a", "answer", "any", "by", "bye", "their" };
		for (String key : texts) {
			test.insert(key);
		}
		TrieNode root = test.getRoot();
		HashMap<Character, TrieNode> child = root.getChildren();
		Iterator<Map.Entry<Character, TrieNode>> itr = child.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry<Character, TrieNode> c = itr.next();

		}
	}

	@Test
	public void search() {
		Trie test = new Trie();
		String keys[] = { "the", "there", "a", "answer", "any", "by", "bye", "their" };
		for (String key : keys) {
			test.insert(key);
		}
		assertTrue(test.search("the"));
		assertFalse(test.search("thiefs"));
	}

	@Test
	public void searchPrefix() {
		Trie test = new Trie();
		String keys[] = { "the", "there", "a", "answer", "any", "by", "bye", "their" };
		for (String key : keys) {
			test.insert(key);
		}
		assertTrue(test.searchPrefix("the"));
		assertTrue(test.searchPrefix("th"));
		assertFalse(test.searchPrefix("base"));
	}

}