package com.atul.udemy;

public class SparseMatrix {
	static int[][] data = new int[][]{
			{1,2,3,4,5,6},
			{7,8,9,10,5,6},
			{1,2,3,4,5,6},
			{1,2,3,4,5,6},
			{1,2,3,4,5,6},
			{1,2,3,4,5,6}
	};
	
	public static void main(String[] args) {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				System.out.print(data[i][j]+" ");
				
			}
			System.out.println();
			
		}
	}
}
