package com.atul.udemy;

public class PalindromeString {

	public boolean isPalindromeRecusrsion(String s, int forward, int backward) {

		if (forward == backward)
			return true;

		if (s.charAt(forward) != s.charAt(backward))
			return false;

		if (forward < backward + 1) {
			return isPalindromeRecusrsion(s, forward + 1, backward - 1);
		}

		return true;
	}

	public static void main(String[] args) {
		PalindromeString palindromeString = new PalindromeString();
		String text = "tenet";
		System.out.println(palindromeString.isPalindromeRecusrsion(text, 0, text.length()-1));
	}

}
