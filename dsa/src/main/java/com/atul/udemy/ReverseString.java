package com.atul.udemy;

public class ReverseString {

	public String toUpperCase(String data) {
		char[] chars = data.toCharArray();
		char[] reverse = new char[chars.length];
		for (int i = 0; i < chars.length; i++) {
			int c = chars[i];
			if (c > 96 && c < 123) {
				chars[i] -= 32;
			}

		}
		return new String(chars);
	}

	public String toLowerCase(String data) {
		char[] chars = data.toCharArray();
		char[] reverse = new char[chars.length];
		for (int i = 0; i < chars.length; i++) {
			int c = chars[i];
			if (c > 64 && c < 91) {
				chars[i] += 32;
			}

		}
		return new String(chars);
	}

	public static void main(String[] args) {
		ReverseString reverseString = new ReverseString();
		System.out.println("Upper Case_" + reverseString.toUpperCase("welcome"));
		System.out.println("Lower Case_" + reverseString.toLowerCase("WELCOME"));
	}
}
