/**
 * 
 */
package com.atul.structure.tree;

/**
 * @author atulgupta
 *
 * BinaryNode.java
 * Oct 15, 2019
 */
public class BinaryNode {
	int data;
	BinaryNode left = null;
	BinaryNode right = null;

	public BinaryNode(int data) {
		this.data = data;
		this.right = null;
		this.left = null;
	}
}


