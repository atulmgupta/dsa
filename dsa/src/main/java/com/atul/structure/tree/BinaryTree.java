package com.atul.structure.tree;

//class BinaryNode {
//	int data;
//	BinaryNode left = null;
//	BinaryNode right = null;
//
//	public BinaryNode(int data) {
//		this.data = data;
//		this.right = null;
//		this.left = null;
//	}
//}
/**
 * 
 * @author atulgupta
 *
 * BinaryTree.java
 * Oct 15, 2019
 */
public class BinaryTree {

	public BinaryNode root;

	public BinaryNode addRecursive(BinaryNode node, int data) {
		if (node == null) {
			node = new BinaryNode(data);
			return node;
		} else {
			if (data < node.data) {
				// left SIde
				node.left = addRecursive(node.left, data);
			} else if (data > node.data) {
				node.right = addRecursive(node.right, data);
			} else {
				return node;
			}
		}
		return node;
	}

	/**
	 * 
	 * @param data
	 */
	public void add(int data) {
		root = addRecursive(root, data);
	}

	/**
	 * 
	 * @param node
	 */
	public void travresePreOrder(BinaryNode node) {
		if (node == null)
			return;
		System.out.print(" " + node.data);
		travresePreOrder(node.left);
		travresePreOrder(node.right);

	}

	/**
	 * 
	 * @param node
	 */
	public void travreseInOrder(BinaryNode node) {
		if (node == null)
			return;
		travreseInOrder(node.left);
		System.out.print(" " + node.data);
		travreseInOrder(node.right);

	}

	/**
	 * 
	 * @param node
	 */
	public void travresePostOrder(BinaryNode node) {
		if (node == null)
			return;
		travresePostOrder(node.left);
		travresePostOrder(node.right);
		System.out.print(" " + node.data);

	}


	public static void main(String[] args) {
		int[] data = new int[] { 6, 4, 8, 3, 5, 7, 9 };
		BinaryTree binaryTree = new BinaryTree();
		for (int i = 0; i < data.length; i++) {
			binaryTree.add(data[i]);
		}
		System.out.println();
		binaryTree.travresePreOrder(binaryTree.root);
		System.out.println();
		binaryTree.travreseInOrder(binaryTree.root);
		System.out.println();
		binaryTree.travresePostOrder(binaryTree.root);
	}
}
