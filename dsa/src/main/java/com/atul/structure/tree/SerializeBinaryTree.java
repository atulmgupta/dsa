/**
 * 
 */
package com.atul.structure.tree;

/**
 * 
 * @author atulgupta
 *
 * SerializeBinaryTree.java
 * Oct 16, 2019
 */
class SerializeBinaryTreeImpl {
	public static String serialize(BinaryNode node) {

		if (node == null)
			return "#";

		StringBuilder builder = new StringBuilder();
		builder.append(node.data);
		builder.append(serialize(node.left));
		builder.append(serialize(node.right));
		return builder.toString();
	}
}

public class SerializeBinaryTree {
	public static void main(String[] args) {
		int[] data = new int[] { 6, 4, 8, 3, 5, 7, 9 };
		BinaryTree binaryTree = new BinaryTree();
		for (int i = 0; i < data.length; i++) {
			binaryTree.add(data[i]);
		}
		System.out.println(SerializeBinaryTreeImpl.serialize(binaryTree.root)); 
	}
}
