package com.atul.structure.stack;

/**
 * STACK Implementation using arrays
 * 
 * push (datatype V) datatype pop() boolean isEmpty() datatype top()
 * 
 * @author atul
 *
 * @param <T>
 */
class StackImpl<T> {
	private int size;
	private int top;
	private T arr[];

	/**
	 * @param max_size
	 */
	public StackImpl(int max_size) {
		this.size = max_size;
		this.top = -1;
		arr = (T[]) new Object[size];

	}

	/**
	 * 
	 * @return
	 */
	public boolean isFull() {
		return this.size == this.top;
	}

	public boolean isEmpty() {
		return top == -1;
	}

	/**
	 * 
	 * @return
	 */
	public int getCapacity() {
		return size;
	}

	public void push(T value) {
		if (isFull()) {
			System.out.println("Stack is Full");
			return;
		}
		arr[++top] = value;
	}

	public T pop() {
		if (isEmpty())
			return null;
		return arr[top--];
	}

}

public class Stack {
	public static void main(String[] args) {
		StackImpl<Integer> stack = new StackImpl<>(15);
		System.out.println("SuccessFully Created a stack");
		System.out.println("Stack Capacity " + stack.getCapacity());

		int[][] demo = new int[10][10];
		for (int i = 0; i < demo.length; i++) {
			for (int j = 0; j < demo[i].length; j++) {
				System.out.print(demo[i][j] + ",");
			}
			System.out.println();
		}

	}
}
