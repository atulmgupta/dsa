package com.atul.structure.linkedList;

/**
 * 
 * @author atulgupta
 *
 * DoublyLinkedList.java
 * Oct 13, 2019
 */
class DoublyLinkedListImpl<T> {
	public class Node {
		public T data;
		public Node leftNode;
		public Node rightNode;

		public Node(T data) {
			this.data = data;
			this.leftNode = null;
			this.rightNode = null;
		}
	}

	public Node headNode;
	public int size;

	public boolean isEmpty() {
		return headNode == null;
	}

	public void deleteByValue(T data) {
		if (isEmpty())
			return;
		
	}

	public void printList() {
		if (isEmpty()) {
			System.out.println("List Is Empty");
			return;
		}

		Node tmp = this.headNode;

		while (tmp != null) {
			System.out.println(tmp.data + "->");
			tmp = tmp.rightNode;
		}
		System.out.println("->null");
	}

}

public class DoublyLinkedList {
	public static void main(String[] args) {
		DoublyLinkedListImpl<Integer> doublyLinkedList = new DoublyLinkedListImpl<>();

	}
}
