/**
 * 
 */
package com.atul.structure.linkedList;

/**
 * @author atulgupta
 *
 * DoublyLinkedListTail.java
 * Oct 15, 2019
 */

class DoublyLinkedListTailImpl<T> {

	public T data;
	public DoublyLinkedNode<T> headNode;
	public DoublyLinkedNode<T> tailNode;
	public int size;

	/**
	 * @return the headNode
	 */
	public DoublyLinkedNode<T> getHeadNode() {
		return headNode;
	}

	/**
	 * @return the tailNode
	 */
	public DoublyLinkedNode<T> getTailNode() {
		return tailNode;
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void insertAtHead(T data) {

		DoublyLinkedNode<T> node = new DoublyLinkedNode<T>(data);
		node.data = data;

		if (!isEmpty()) {
			node.nextNode = this.headNode;
		} else {

			tailNode = node;
		}
		this.headNode = node;
		size++;
	}

	public void insertAtEnd(T data) {
		if (isEmpty()) {
			insertAtHead(data);
			return;
		}

		DoublyLinkedNode<T> node = new DoublyLinkedNode<T>(data);
		node.data = data;
		node.prevNode = tailNode;
		tailNode.nextNode = node;
		tailNode = node;
		size++;

	}

	/**
	 * 
	 */
	public void printList() {

		if (isEmpty()) {
			System.out.println("List Is Empty");
			return;
		}

		DoublyLinkedNode node = headNode;
		System.out.print("List : null <- ");

		while (node.nextNode != null) {
			System.out.print(node.data.toString() + " <-> ");
			node = node.nextNode;

		}

		System.out.println(node.data.toString() + " -> null");
	}

	/**
	 * 
	 */
	public void deleteAtHead() {
		if (isEmpty())
			return;
		if (size == 1) {
			this.headNode = null;
			this.tailNode = null;
			return;

		}
		
		
		
	}

	public void deleteAtTail() {

	}

}

public class DoublyLinkedListTail {
	public static void main(String[] args) {
		DoublyLinkedListTailImpl<Integer> doublyLinkedListTailImpl = new DoublyLinkedListTailImpl();

		doublyLinkedListTailImpl.insertAtHead(10);
		doublyLinkedListTailImpl.insertAtHead(11);
		doublyLinkedListTailImpl.insertAtHead(12);
		doublyLinkedListTailImpl.insertAtHead(13);
		doublyLinkedListTailImpl.printList();
		doublyLinkedListTailImpl.insertAtEnd(9);
		doublyLinkedListTailImpl.insertAtEnd(8);
		doublyLinkedListTailImpl.insertAtEnd(7);
		doublyLinkedListTailImpl.printList();
	}
}
