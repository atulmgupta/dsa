package com.atul.structure.linkedList;

/**
 *
 * Singly Linked List Implementation
 * @author atulgupta
 *
 * SinglyLinkedList.java
 * Oct 13, 2019
 *
 *  Operations	Descriptions
    insertAtEnd(data)	inserts an element at the end of the linked list
    insertAtHead(data)	inserts an element at the start/head of the linked list
    delete(data)		deletes an element from the linked list
    deleteAtHead()		deletes the first element of the list
    deleteAtEnd()		deletes the last element of the list
    Search(data)		searches an element from the linked list
    isEmpty()			returns True if the linked list is empty, otherwise returns false
 */
class SinglyLinkedListImpl<T> {
    public class Node {
        public T data;
        public Node nextNode;
    }

    public Node headNode;
    public int size;
    /**
     *
     */
    public SinglyLinkedListImpl() {
        headNode = null;
        size = 0;

    }
    /**
     *
     * @param data
     */
    public void insertAtEnd(T data) {
        Node node = null;

        if (!isEmpty()) {
            node = headNode;
            while (node.nextNode != null) {
                node = node.nextNode;
            }
        }

        Node newNode = new Node();
        newNode.data = data;

        if (node == null) {
            headNode = newNode;
        } else {
            node.nextNode = newNode;
        }

        size++;

    }
    /**
     *
     * @param data
     */
    public void insertAtHead(T data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.nextNode = headNode;
        headNode = newNode;
        size++;

    }
    /**
     *
     * @param data
     */
    public void delete(T data) {
        if (this.isEmpty())
            return;

        Node tmp = this.headNode;
        Node previousNode = null;
        Node currentNode = null;
        while (tmp != null) {
            if (tmp.data == data) {
                currentNode = tmp;
                break;
            }
            previousNode = tmp;
            tmp = tmp.nextNode;
        }

        if (currentNode != null) {
            if (previousNode != null) {
                previousNode.nextNode = currentNode.nextNode;

            } else {
                currentNode.nextNode = null;
            }
        }
        return;
    }
    /**
     *
     */
    public void deleteAtHead() {
        if (!isEmpty()) {
            Node node = headNode;
            headNode = node.nextNode;
            size--;
        }
    }
    /**
     *
     */
    public void deleteAtEnd() {
        if (!isEmpty()) {
            Node node = headNode;
            while (node.nextNode.nextNode != null) {
                node = node.nextNode;
                //				System.out.println(node.data);
            }

            node.nextNode = null;
            size--;
        }
    }
    /**
     *
     * @param data
     * @return
     */
    public boolean Search(T data) {
        if (!isEmpty()) {
            Node node = headNode;
            while (node != null) {
                if (node.data == data)
                    return true;

                node = node.nextNode;
            }
        }
        return false;
    }
    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return headNode == null;

    }
    /**
     *
     */
    public void printList() {
        if (!isEmpty()) {
            Node node = headNode;
            while (node != null) {
                System.out.print(node.data + "->");
                node = node.nextNode;
            }
            System.out.print("null");
            System.out.println();
        } else {
            System.out.println("Linked List is empty");
        }

    }
}
/**
 *
 * @author atulgupta
 *
 * SinglyLinkedList.java
 * Oct 13, 2019
 */
public class SinglyLinkedList {
    public static void main(String[] args) {
        SinglyLinkedListImpl<Integer> sll = new SinglyLinkedListImpl<Integer>();
        System.out.println(sll.isEmpty());
        sll.insertAtHead(5);
        sll.printList();
        sll.insertAtEnd(15);
        for (int i = 0; i < 15; i++) {
            sll.insertAtEnd(i * 2);

        }
        sll.printList();
        sll.deleteAtHead();
        sll.deleteAtHead();
        sll.deleteAtHead();
        sll.printList();

        sll.deleteAtEnd();
        sll.deleteAtEnd();
        sll.deleteAtEnd();
        sll.deleteAtEnd();
        sll.printList();

        sll.delete(10);
        sll.printList();
        System.out.println(sll.Search(20));
        System.out.println(sll.Search(21));
    }
}
