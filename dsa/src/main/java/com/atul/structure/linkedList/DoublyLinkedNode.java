/**
 * 
 */
package com.atul.structure.linkedList;

/**
 * @author atulgupta
 *
 * DoublyLinkedNode.java
 * Oct 15, 2019
 */
public class DoublyLinkedNode<T> {
	public T data;
	public DoublyLinkedNode nextNode;
	public DoublyLinkedNode prevNode;

	/**
	 * @param data
	 */
	public DoublyLinkedNode(T data) {
		super();
		this.data = data;
		this.nextNode = null;
		this.prevNode = null;
	}

}
