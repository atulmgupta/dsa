package com.atul.structure.trie;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Trie {

	private static final Logger log = LoggerFactory.getLogger(Trie.class);

	private TrieNode root;

	public Trie() {
		root = new TrieNode();
	}

	public TrieNode getRoot() {
		return root;
	}

	public void setRoot(TrieNode root) {
		this.root = root;
	}

	public void printTrie() {
		
	}

	// Implement Insert
	public void insert(String word) {
		log.info("Inserting word {}", word);
		HashMap<Character, TrieNode> children = root.children;
		TrieNode t = null;
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
//			log.info("Inserting Char {}", c);
			if (children.containsKey(c)) {
				t = children.get(c);
			} else {
				t = new TrieNode(c);
				children.put(c, t);
			}
			children = t.children;

			// set leaf node
			if (i == word.length() - 1)
				t.isEndofWord = true;
		}
	}

	// Implement Delete
	public void delete(String word) {

	}
	public boolean searchPrefix(String word) {

		TrieNode node = searchNode(word);
		if (node != null) {
			return true;
		}
		return false;
	}
	// Implement Search
	public boolean search(String word) {

		TrieNode node = searchNode(word);
		if (node != null && node.isEndofWord) {
			return true;
		}
		return false;
	}

	public TrieNode searchNode(String str) {
		HashMap<Character, TrieNode> children = root.getChildren();
		TrieNode t = null;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (children.containsKey(c)) {
				t = children.get(c);
				children = t.getChildren();
			} else {
				return null;
			}
		}
		return t;
	}

	
	
}
