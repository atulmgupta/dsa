package com.atul.structure.trie;

import java.util.HashMap;

/**
 * Trie Node For Auto Complete Suggestions
 */
public class TrieNode {
	char character;
	boolean isEndofWord;
	HashMap<Character, TrieNode> children = new HashMap<Character, TrieNode>();

	public TrieNode() {

	}

	public TrieNode(char c) {
		this.character = c;

	}

	public char getCharacter() {
		return character;
	}

	public void setCharacter(char character) {
		this.character = character;
	}

	public boolean isEndofWord() {
		return isEndofWord;
	}

	public void setEndofWord(boolean isEndofWord) {
		this.isEndofWord = isEndofWord;
	}

	public HashMap<Character, TrieNode> getChildren() {
		return children;
	}

	public void setChildren(HashMap<Character, TrieNode> children) {
		this.children = children;
	}
}
