package com.atul.hackerRank.dataStructures;

public class SinglyLinkedListNode {

	int data;
	SinglyLinkedListNode next;

	public SinglyLinkedListNode() {
		super();
	}

	public SinglyLinkedListNode(int data) {
		this.data = data;
		this.next = null;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public SinglyLinkedListNode getNext() {
		return next;
	}

	public void setNext(SinglyLinkedListNode next) {
		this.next = next;
	}
}
