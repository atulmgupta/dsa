package com.atul.hackerRank.medium;

import java.util.Scanner;

/**
 * It's New Year's Day and everyone's in line for the Wonderland rollercoaster ride! There are a number of people queued up, and each person wears a sticker indicating their initial position in the queue. Initial positions increment by  from  at the front of the line to  at the back.
 * <p>
 * Any person in the queue can bribe the person directly in front of them to swap positions. If two people swap positions, they still wear the same sticker denoting their original places in line. One person can bribe at most two others. For example, if  and  bribes , the queue will look like this: .
 * <p>
 * Fascinated by this chaotic queue, you decide you must know the minimum number of bribes that took place to get the queue into its current state!
 * <p>
 * Function Description
 * <p>
 * Complete the function minimumBribes in the editor below. It must print an integer representing the minimum number of bribes necessary, or Too chaotic if the line configuration is not possible.
 * <p>
 * minimumBribes has the following parameter(s):
 * <p>
 * q: an array of integers
 * Input Format
 * <p>
 * The first line contains an integer , the number of test cases.
 * <p>
 * Each of the next  pairs of lines are as follows:
 * - The first line contains an integer , the number of people in the queue
 * - The second line has  space-separated integers describing the final state of the queue.
 * <p>
 * Constraints
 * <p>
 * Subtasks
 * <p>
 * For  score
 * For  score
 * <p>
 * Output Format
 * <p>
 * Print an integer denoting the minimum number of bribes needed to get the queue into its final state. Print Too chaotic if the state is invalid, i.e. it requires a person to have bribed more than  people.
 * <p>
 * Sample Input
 * <p>
 * 2
 * 5
 * 2 1 5 3 4
 * 5
 * 2 5 1 3 4
 * Sample Output
 * <p>
 * 3
 * Too chaotic
 */
public class NewYearChaos {
    static void minimumBribes(int[] q) {
        int length = q.length;
        int initial = 1;
        int bribes = 0;
        for (int i = 0; i < length; i++) {
            if (q[i] != initial) {
                if (i - length > 3) {
                    System.out.println("Too Chaotic");
                    return;
                }


                bribes++;
                initial++;
            }
        }

        System.out.println("Minimum Bribes required :" + bribes);

    }

    public static void main(String[] args) {
        int[] data = {2, 5, 1, 3, 4};
        minimumBribes(data);
    }
}
