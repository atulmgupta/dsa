package com.atul.leetcode.top.array;

/**
 * 
 * @author atulgupta
 *
 *
 * Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.
	
	Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
	
	Example 1:
	
	Given nums = [1,1,2],
	
	Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
	
	It doesn't matter what you leave beyond the returned length.
 */

class RemoveDuplicatesFromSortedArraySolution {
	public int removeDuplicates(int[] nums) {
		if (nums.length == 0)
			return 0;
		if (nums.length == 1)
			return 1;
		int j = 0;
		for (int i = 0; i < nums.length - 1; i++) {

			if (nums[i] != nums[i + 1]) {
				nums[j++] = nums[i];
			}

		}
		nums[j++] = nums[nums.length - 1];

		return j;

	}
}

public class RemoveDuplicatesFromSortedArray {
	public static void main(String[] args) {
		RemoveDuplicatesFromSortedArraySolution arraySolution = new RemoveDuplicatesFromSortedArraySolution();
		int[] nums = { 1, 1, 2 };
		System.out.println(arraySolution.removeDuplicates(nums));
	}
}
