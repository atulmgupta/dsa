package com.atul.leetcode.easy;

/** * question_id :243
* question__title :Shortest Word Distance
* question__title_slug :shortest-word-distance
* question__url :https://leetcode.com/problems/shortest-word-distance/
* Level  :Easy
* 
* Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.

	Example:
	Assume that words = ["practice", "makes", "perfect", "coding", "makes"].
	
	Input: word1 = “coding”, word2 = “practice”
	Output: 3
	Input: word1 = "makes", word2 = "coding"
	Output: 1
**/

class ShortestWordDistanceSolution {
	public int shortestDistance(String[] words, String word1, String word2) {
		int p1 = -1;
		int p2 = -1;
		int minDistance = Integer.MAX_VALUE;

		for (int i = 0; i < words.length; i++) {
			if (words[i].equals(word1)) {
				p1 = i;
			}
			if (words[i].equals(word2)) {
				p2 = i;
			}
			if (p1 != -1 && p2 != -1) {
				minDistance = Math.min(minDistance, Math.abs(p1 - p2));
			}
		}

		return minDistance;

	}
}

public class ShortestWordDistance {
	public static void main(String[] args) {
		ShortestWordDistanceSolution solution = new ShortestWordDistanceSolution();
		String[] words = { "practice", "makes", "perfect", "coding", "makes" };
		String word1 = "coding";
		String word2 = "practice";
		System.out.println(solution.shortestDistance(words, word1, word2));

		word1 = "makes";
		word2 = "coding";
		System.out.println(solution.shortestDistance(words, word1, word2));

		String[] words2 = { "a", "a", "b", "b" };
		String word3 = "a";
		String word4 = "b";
		System.out.println(solution.shortestDistance(words2, word3, word4));
	}
}