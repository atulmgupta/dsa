package com.atul.leetcode.easy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/** * question_id :205
* question__title :Isomorphic Strings
* question__title_slug :isomorphic-strings
* question__url :https://leetcode.com/problems/isomorphic-strings/
* Level  :Easy
* 
* Given two strings s and t, determine if they are isomorphic.

	Two strings are isomorphic if the characters in s can be replaced to get t.
	
	All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character but a character may map to itself.
	
	Example 1:
	
	Input: s = "egg", t = "add"
	Output: true
	Example 2:
	
	Input: s = "foo", t = "bar"
	Output: false
	Example 3:
	
	Input: s = "paper", t = "title"
	Output: true
	Note:
	You may assume both s and t have the same length.
**/

class IsomorphicStringsSolution {
	public boolean isIsomorphic(String s, String t) {
		if (s.length() != t.length())
			return false;

		Map<Character, Character> map = new HashMap<Character, Character>();
		Set<Character> set = new HashSet<>();
		for (int i = 0; i < s.length(); i++) {

			if (map.containsKey(s.charAt(i))) {
				if (!map.get(s.charAt(i)).equals(t.charAt(i))) {
					return false;
				}
			} else {
				if (set.contains(t.charAt(i)))
					return false;
				map.put(s.charAt(i), t.charAt(i));
				set.add(t.charAt(i));
			}

		}
		return true;
	}
}

public class IsomorphicStrings {
	public static void main(String[] args) {
		String s = "egg";
		String t = "add";
		IsomorphicStringsSolution solution = new IsomorphicStringsSolution();
		System.out.println(solution.isIsomorphic(s, t));

		s = "foo";
		t = "bar";

		System.out.println(solution.isIsomorphic(s, t));
		s = "paper";
		t = "title";
		System.out.println(solution.isIsomorphic(s, t));

		s = "ab";
		t = "aa";
		System.out.println(solution.isIsomorphic(s, t));
	}
}