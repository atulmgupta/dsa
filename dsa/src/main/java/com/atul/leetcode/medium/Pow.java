package com.atul.leetcode.medium;

/**
 * question_id :50
 * 
 * @author atulgupta
 *
 *         Implement pow(x, n), which calculates x raised to the power n (xn).
 *
 *         Example 1: Input: 2.00000, 10 Output: 1024.00000
 * 
 * 
 *         Example 2 Input: 2.00000, -2 Output: 0.25000 Explanation: 2-2 = 1/22
 *         = 1/4 = 0.25
 *
 */
class Solution {
	public double fastPow(double x, int n) {
		if (n == 0) {
			return 1.0;
		}
		double half = fastPow(x, n / 2);
		if (n % 2 == 0) {
			return half * half;
		} else {
			return half * half * x;
		}

	}

	public double myPow(double x, int n) {
		if (n < 0) {
			return 1 / fastPow(x, n);
		}
		return fastPow(x, n);

	}
}

public class Pow {
	public static void main(String[] args) {
		Solution solution = new Solution();
//		System.out.println(solution.myPow(2.00000, 10));
//		System.out.println(solution.myPow(2.10000, 3));
//		System.out.println(solution.myPow(2.00000, 2));
		System.out.println(solution.myPow(2.00000, -2));
	}
}