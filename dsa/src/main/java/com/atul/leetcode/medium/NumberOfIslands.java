package com.atul.leetcode.medium;

/** * question_id :200
* question__title :Number of Islands
* question__title_slug :number-of-islands
* question__url :https://leetcode.com/problems/number-of-islands/
* Level  :Medium
* 
* Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. 
* An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. 
* You may assume all four edges of the grid are all surrounded by water.
* 
* Example 1:

	Input:
	11110
	11010
	11000
	00000
	
	Output: 1
	
	
	*************
	Example 2:

	Input:
	11000
	11000
	00100
	00011
	
	Output: 3
**/

class NumberOfIslandsSolution {

	class unionFind {
		int count;
		int[] parent;
		int[] rank;

		public unionFind(int n) {
			rank = new int[n];
			parent = new int[n];
			this.count = n;
			makeSet();
		}

		void makeSet() {
			// Initially, all elements are in their
			// own set.
			for (int i = 0; i < count; i++)
				parent[i] = i;
		}

		int find(int x) {
			if (parent[x] != x) {
				// if x is not the parent of itself,
				// then x is not the representative of
				// its set.
				// so we recursively call Find on its parent
				// and move i's node directly under the
				// representative of this set
				return find(parent[x]);
			}

			return x;
		}

		void union(int x, int y) {
			// Find the representatives (or the root nodes)
			// for x an y
			int xRoot = find(x);
			int yRoot = find(y);

			// Elements are in the same set, no need
			// to unite anything.
			if (xRoot == yRoot)
				return;

			// If x's rank is less than y's rank
			// Then move x under y so that depth of tree
			// remains less
			if (rank[xRoot] < rank[yRoot])
				parent[xRoot] = yRoot;

			// Else if y's rank is less than x's rank
			// Then move y under x so that depth of tree
			// remains less
			else if (rank[yRoot] < rank[xRoot])
				parent[yRoot] = xRoot;

			else // Else if their ranks are the same
			{
				// Then move y under x (doesn't matter
				// which one goes where)
				parent[yRoot] = xRoot;

				// And increment the result tree's
				// rank by 1
				rank[xRoot] = rank[xRoot] + 1;
			}
		}
	}

	public int numIslands(int[][] a) {
		  int n = a.length; 
	        int m = a[0].length; 
	  
	        unionFind dus = new unionFind(n*m); 
	  
	        /* The following loop checks for its neighbours 
	           and unites the indexes  if both are 1. */
	        for (int j=0; j<n; j++) 
	        { 
	            for (int k=0; k<m; k++) 
	            { 
	                // If cell is 0, nothing to do 
	                if (a[j][k] == 0) 
	                    continue; 
	  
	                // Check all 8 neighbours and do a union 
	                // with neighbour's set if neighbour is  
	                // also 1 
	                if (j+1 < n && a[j+1][k]==1) 
	                    dus.union(j*(m)+k, (j+1)*(m)+k); 
	                if (j-1 >= 0 && a[j-1][k]==1) 
	                    dus.union(j*(m)+k, (j-1)*(m)+k); 
	                if (k+1 < m && a[j][k+1]==1) 
	                    dus.union(j*(m)+k, (j)*(m)+k+1); 
	                if (k-1 >= 0 && a[j][k-1]==1) 
	                    dus.union(j*(m)+k, (j)*(m)+k-1); 
	                if (j+1<n && k+1<m && a[j+1][k+1]==1) 
	                    dus.union(j*(m)+k, (j+1)*(m)+k+1); 
	                if (j+1<n && k-1>=0 && a[j+1][k-1]==1) 
	                    dus.union(j*m+k, (j+1)*(m)+k-1); 
	                if (j-1>=0 && k+1<m && a[j-1][k+1]==1) 
	                    dus.union(j*m+k, (j-1)*m+k+1); 
	                if (j-1>=0 && k-1>=0 && a[j-1][k-1]==1) 
	                    dus.union(j*m+k, (j-1)*m+k-1); 
	            } 
	        } 
	  
	        // Array to note down frequency of each set 
	        int[] c = new int[n*m]; 
	        int numberOfIslands = 0; 
	        for (int j=0; j<n; j++) 
	        { 
	            for (int k=0; k<m; k++) 
	            { 
	                if (a[j][k]==1) 
	                { 
	  
	                    int x = dus.find(j*m+k); 
	  
	                    // If frequency of set is 0,  
	                    // increment numberOfIslands 
	                    if (c[x]==0) 
	                    { 
	                        numberOfIslands++; 
	                        c[x]++; 
	                    } 
	  
	                    else
	                        c[x]++; 
	                } 
	            } 
	        } 
	        return numberOfIslands;

	}
}

public class NumberOfIslands {
	public static void main(String[] args) {
		NumberOfIslandsSolution solution = new NumberOfIslandsSolution();
		int M[][] = new int[][] { { 1, 1, 0, 0, 0 }, { 0, 1, 0, 0, 1 }, { 1, 0, 0, 1, 1 }, { 0, 0, 0, 0, 0 },
				{ 1, 0, 1, 0, 1 } };
				System.out.println("Number of Islands is: " + 
						solution.numIslands(M)); 
	}
}