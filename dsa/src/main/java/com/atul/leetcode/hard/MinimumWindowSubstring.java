package com.atul.leetcode.hard;

/** * question_id :76
* question__title :Minimum Window Substring
* question__title_slug :minimum-window-substring
* question__url :https://leetcode.com/problems/minimum-window-substring/
* Level  :Hard
* 
* 
* Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).

	Example:
	
	Input: S = "ADOBECODEBANC", T = "ABC"
	Output: "BANC"
	Note:
	
	If there is no such window in S that covers all characters in T, return the empty string "".
	If there is such window, you are guaranteed that there will always be only one unique minimum window in S.
**/

class MinimumWindowSubstringSolution {
	public String minWindow(String s, String t) {
		if (t.length() > s.length())
			return "";

	}
}

public class MinimumWindowSubstring {
	public static void main(String[] args) {
		MinimumWindowSubstringSolution solution = new MinimumWindowSubstringSolution();
		String s = "ADOBECODEBANC";
		String t = "ABC";
		System.out.println(solution.minWindow(s, t));
	}
}