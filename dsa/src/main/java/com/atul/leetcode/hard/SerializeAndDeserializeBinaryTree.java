package com.atul.leetcode.hard;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/** * question_id :297
* question__title :Serialize and Deserialize Binary Tree
* question__title_slug :serialize-and-deserialize-binary-tree
* question__url :https://leetcode.com/problems/serialize-and-deserialize-binary-tree/
* Level  :Hard
* 
* Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, 
* or transmitted across a network connection link to be reconstructed later in the same or another computer environment.
* 
* Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. 
* You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.
* 
* Example: 

	You may serialize the following tree:
	
	    1
	   / \
	  2   3
	     / \
	    4   5
	
	as "[1,2,3,null,null,4,5]"
**/
class Node {
	int data;
	Node left;
	Node right;

	public Node(int data) {
		this.data = data;
		this.left = null;
		this.right = null;
	}

}

class BinaryTree {

	Node root;

	public Node addRecursive(Node node, int data) {
		if (node == null) {
			node = new Node(data);
			return node;
		} else {
			if (data < node.data) {
				node.left = addRecursive(node.left, data);
			} else if (data > node.data) {
				node.right = addRecursive(node.right, data);
			} else {
				return node;
			}
		}
		return node;
	}

	public void add(int data) {
		root = addRecursive(root, data);

	}

}

class Codec {

	// Encodes a tree to a single string.

	public String rserialize(Node root, String str) {
		if (root == null) {
			str += "null,";
		} else {
			str += String.valueOf(root.data) + ",";
			str = rserialize(root.left, str);
			str = rserialize(root.right, str);
		}

		return str;
	}

	public String serialize(Node node) {
		return rserialize(node, "");

	}
	public Node rdeserialize(List<String> l) {
		if(l.get(0).equals("null")) {
			l.remove(0);
			return null;
		}
		
		Node root = new Node(Integer.valueOf(l.get(0)));
		l.remove(0);
		root.left = rdeserialize(l);
		root.right = rdeserialize(l);
		return root;
	}
	// Decodes your encoded data to tree.
	public Node deserialize(String data) {

		String[] data_array = data.split(",");
		java.util.List<String> data_list = new LinkedList<>(Arrays.asList(data_array));

		return rdeserialize(data_list);

	}
}

public class SerializeAndDeserializeBinaryTree {

	public static void main(String[] args) {
		Codec codec = new Codec();
		BinaryTree tree = new BinaryTree();
		int[] data = new int[] { 6, 4, 8, 3, 5, 7, 9 };
		for (int i = 0; i < data.length; i++) {
			tree.add(data[i]);
		}
		System.out.println();
		String result = codec.serialize(tree.root);		
		System.err.println(result);
		Node root = codec.deserialize(result);
		System.err.println();
	}
}
