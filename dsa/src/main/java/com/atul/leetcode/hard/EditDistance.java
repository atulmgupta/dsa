package com.atul.leetcode.hard;

import org.springframework.util.Assert;

/** * question_id :72
* question__title :Edit Distance
* question__title_slug :edit-distance
* question__url :https://leetcode.com/problems/edit-distance/
* Level  :Hard
* 
* 
* Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.

	You have the following 3 operations permitted on a word:
	
	Insert a character
	Delete a character
	Replace a character
	Example 1:
	
	Input: word1 = "horse", word2 = "ros"
	Output: 3
	Explanation: 
	horse -> rorse (replace 'h' with 'r')
	rorse -> rose (remove 'r')
	rose -> ros (remove 'e')
	
	Example 2:

	Input: word1 = "intention", word2 = "execution"
	Output: 5
	Explanation: 
	intention -> inention (remove 't')
	inention -> enention (replace 'i' with 'e')
	enention -> exention (replace 'n' with 'x')
	exention -> exection (replace 'n' with 'c')
	exection -> execution (insert 'u')
		
	
**/

class EditDistanceSolution {

	int min(int x, int y, int z) {
		if (x <= y && x <= z)
			return x;
		if (y <= x && y <= z)
			return y;
		else
			return z;
	}

	public int minDistance(String word1, String word2) {
		int n = word1.length();
		int m = word2.length();

		if (n * m == 0)
			return n + m;

		int[][] d = new int[n + 1][m + 1];
		for (int i = 0; i < n + 1; i++) {
			d[i][0] = i;

		}

		for (int j = 0; j < m + 1; j++) {
			d[0][j] = j;
		}

		for (int i = 1; i < n + 1; i++) {
			for (int j = 1; j < m + 1; j++) {

				int diagonal = d[i - 1][j - 1];
				int upper = d[i][j - 1];
				int left = d[i - 1][j];

				// if Words are same
				if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
					d[i][j] = diagonal;
				} else {
					// If Words Are Not same
					d[i][j] = 1 + min(diagonal, left, upper);
				}

			}

		}
		return d[n][m];

	}
}

public class EditDistance {
	public static void main(String[] args) {
		EditDistanceSolution solution = new EditDistanceSolution();
		String word1 = "horse";
		String word2 = "ros";
		System.out.println(solution.minDistance(word1, word2));
		word1 = "intention";
		word2 = "execution";
		System.out.println(solution.minDistance(word1, word2));
	}
}