package com.atul.algorithm.cache;

import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Add member to front of queue
 */
public class LRU {

    static Deque<Integer> dq;
    static HashSet<Integer> map;
    static int csize;

    public LRU(int x) {
        dq = new LinkedList<Integer>();
        map = new HashSet<Integer>();
        csize = x;
    }

    public void refer(int x) {
        if (!map.contains(x)) {
            if (map.size() == csize) {
                int lastItem = dq.removeLast();
                map.remove(lastItem);
            }

        } else {
            int index = 0, i = 0;
            Iterator<Integer> itr = dq.iterator();
            while (itr.hasNext()) {
                if (itr.next() == x) {
                    index = i;
                    break;
                }
                i++;
            }
            dq.remove(index);
        }
        dq.push(x);
        map.add(x);
    }

    public void display() {
        Iterator<Integer> itr = dq.iterator();
        while (itr.hasNext()) {
            System.out.print(itr.next() + " ");
        }
    }

    public static void main(String[] args) {
        LRU cache = new LRU(4);
        cache.refer(1);
        cache.refer(2);
        cache.refer(3);
        cache.refer(1);
        cache.refer(4);
        cache.refer(5);
        cache.display();
    }
}
