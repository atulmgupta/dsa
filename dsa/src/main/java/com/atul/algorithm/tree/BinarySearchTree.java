package com.atul.algorithm.tree;

/**
 * 
 * @author atulgupta
 *
 * BinarySearchTree.java
 * Oct 13, 2019
 */
public class BinarySearchTree {
	Node root;

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	public boolean isEmpty() {
		return this.root == null;

	}

	public boolean insertIterative(int data) {

		if (isEmpty()) {
			root = new Node(data);
			return true;
		}

		Node current = root;

		while (current != null) {
			Node leftChild = current.getLeftChild();
			Node rightChild = current.getRightChild();
			if (data <= current.getData()) {
				if (leftChild == null) {
					leftChild = new Node(data);
					current.setLeftChild(leftChild);
					return true;
				}
				current = leftChild;
			} else {
				if (rightChild == null) {
					rightChild = new Node(data);
					current.setRightChild(rightChild);
					return true;
				}
				current = rightChild;
			}
		}
		return false;
	}

	public Node insertRecursive(Node currentNode, int data) {

		if (currentNode == null) {
			return new Node(data);
		}

		if (data < currentNode.getData()) {
			currentNode.setLeftChild(insertRecursive(currentNode.getLeftChild(), data));
		} else if (data > currentNode.getData()) {
			currentNode.setRightChild(insertRecursive(currentNode.getRightChild(), data));
		}
		return currentNode;
	}

	public boolean addRecursive(int data) {
		root = insertRecursive(this.root, data);
		return true;
	}

	public void printTree(Node currentNode) {
		if (currentNode == null)
			return;
		System.out.println(currentNode.getData() + " ");
		printTree(currentNode.getLeftChild());
		printTree(currentNode.getRightChild());

	}
}
