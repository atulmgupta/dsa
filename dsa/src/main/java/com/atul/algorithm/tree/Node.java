package com.atul.algorithm.tree;

/**
 * 
 * @author atulgupta
 *
 * Node.java
 * Oct 13, 2019
 */
public class Node {

	private int data;

	private Node leftChild;
	private Node rightChild;

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public Node getRightChild() {
		return rightChild;
	}

	public void setRightChild(Node rightChild) {
		this.rightChild = rightChild;
	}

	public Node getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(Node leftChild) {
		this.leftChild = leftChild;
	}

	Node(int data) {
		this.data = data;
		this.leftChild = null;
		this.rightChild = null;
	}
}
